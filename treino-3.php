<?php
// criei uma variável que guardará todas as informações do array
$hierarquia = array(
    //iniciando o array dentro de outro, começando pelo CEO que tera CTO>DEV e Gerente>Atendimento>implementação.
    array(
        'nome_cargo'=>'CEO',
        'nome'=>'André Furmahn',
        'subordinados'=>array(
            array(
                //Inicio a arvore de array
                'nome_cargo'=>'CTO',
                'nome'=>'Fabricio',
                //inicio os subordinados do CTO
                'subordinados'=>array(
                    array(
                        'nome_cargo'=>'Desenvolvedor',
                        'nome'=>'Maxuell Stalin',
                    ),
                    array(
                        'nome_cargo'=>'Desenvolvedor',
                        'nome'=>'Guilherme',
                    ),
                    array(
                        'nome_cargo'=>'Suporte',
                        'nome'=>'Thainara',
                    ),
                    array(
                        'nome_cargo'=>'Suporte',
                        'nome'=>'Matheus'
                    ),
                //termino dos subordinados do CTO
                )


            ),
            //Fim do grupo CTO e inicio ao grupo Gerente
            //Inicio Gerente Operacional que pertence ao CEO
                array(
                    'nome_cargo'=>'Gerente Operacional',
                    'nome'=>'Livia',
                    'subordinados'=>array(
                        array(
                            'nome_cargo'=>'Atendimento',
                            'nome'=>'Beatriz',
                        ),
                        array(
                            'nome_cargo'=>'Atendimento',
                            'nome'=>'Nicole',
                        ),
                        array(
                            'nome_cargo'=>'Implementação',
                            'nome'=>'Kayke'
                        ),
                        array(
                            'nome_cargo'=>'Implementação',
                            'nome'=>'Stefany'
                        )

                    )
                )
    )
)
);


function exibe($cargos){

    $html = '<ul>';

    foreach($cargos as $cargo){
        $html .= "<li>";
        $html .= $cargo['nome_cargo'] . "<br>". "Nome:". " " . $cargo['nome'];
        $html .= "</li>";

        if (isset($cargo['subordinados']) && count($cargo['subordinados']) > 0){

            $html .= exibe($cargo['subordinados']);
            

        }

    }

    $html .= '</ul>';

    return $html;
}
echo exibe($hierarquia);
